var express = require('express');
var http = require('http');
var app = express();
require('dotenv').config({ debug: process.env.DEBUG });

var connection = require('express-myconnection');
var mysql = require('mysql');
app.use(express.json());

var user = require('./routes/user');
app.set('port', process.env.PORT || 3000);

app.use(
    connection(mysql, {
        host: process.env.DB_HOST || 'localhost',
        user: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        port: process.env.DB_PORT || 3306,
        database: process.env.DB_DATABASE
    }, 'pool')
);

app.get('/api/user_list', user.getUserList);
app.get('/api/get_user/:id', user.getUser);
app.post('/api/add_user', user.saveUser);
app.put('/api/edit_user/:id', user.editUser);
app.delete('/api/delete_user/:id', user.deleteUser);

http.createServer(app).listen(app.get('port'));

console.log('server running at: http://localhost:' + app.get('port'))