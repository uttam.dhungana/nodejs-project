//Get User List

exports.getUserList = function (req, res) {
    req.getConnection(function (err, connection) {
        var query = connection.query('SELECT * FROM users', (err, rows) => {
            if (err) throw err;
            res.send(rows);
        });
    });

};

exports.getUser = function (req, res) {
    let id = req.params.id;
    req.getConnection(function (err, connection) {
        var query = connection.query('SELECT * FROM users WHERE id = ?', [id], (err, rows) => {
            if (err) throw err;
            res.send(rows);
        });
    });
};

exports.saveUser = function (req, res) {
    let input = req.body;
    req.getConnection(function (err, connection) {
        var data = {
            name: input.name,
            address: input.address,
            email: input.email,
            phone: input.phone
        };
        var query = connection.query("INSERT INTO users set ? ", data, (err, rows) => {
            if (err) throw err;
            res.send(rows);
        });
    });
};

exports.editUser = function (req, res) {
    let input = req.body;
    let id = req.params.id;
    req.getConnection(function (err, connection) {
        var data = {
            name: input.name,
            address: input.address,
            email: input.email,
            phone: input.phone
        };
        var query = connection.query("UPDATE users set ? WHERE id = ? ", [data, id], (err, rows) => {
            if (err) throw err;
            res.send(rows);
        });
    });
};


exports.deleteUser = function (req, res) {
    let id = req.params.id;
    req.getConnection(function (err, connection) {
        connection.query("DELETE FROM users  WHERE id = ? ", [id], function (err, rows) {

            if (err) throw err;
            res.send(rows)
        });

    });
};



