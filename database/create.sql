create database node_test;
use node_test;

CREATE TABLE IF NOT EXISTS users (
    id INT AUTO_INCREMENT primary key,
    name VARCHAR(100) NOT NULL,
    email  VARCHAR(50),
    address  VARCHAR(255),
    phone  VARCHAR(15),
    status TINYINT NOT NULL default 1
)  ENGINE=INNODB;
